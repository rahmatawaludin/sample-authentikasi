<!-- resources/views/auth/login.blade.php -->

<form method="POST" action="/auth/login">

    @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! csrf_field() !!}

    <div>
        Username
        <input type="text" name="username" value="{{ old('username') }}">
    </div>

    <div>
        Password
        <input type="password" name="password" id="password">
    </div>

    <div>
        <input type="checkbox" name="remember"> Remember Me
    </div>

    <div>
        <button type="submit">Login</button>
        <a href="/password/email">Forgot password?</a>
    </div>
    <div>
        <a href="/auth/facebook">Login dengan Facebook</a>
    </div>
</form>
