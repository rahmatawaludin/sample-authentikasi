<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Event;
use App\User;

class EventPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Event $event)
    {
        return $user->id == $event->organizer_id;
    }

    public function join(User $user, Event $event)
    {
        return $user->role == 'participant' && $event->published;
    }
}
