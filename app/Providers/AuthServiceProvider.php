<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\Event::class => \App\Policies\EventPolicy::class,
        \App\Organization::class => \App\Policies\OrganizationPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);

        $gate->define('be-organizer', function ($user) {
            return $user->role == 'organizer';
        });

        $gate->define('be-participant', function ($user) {
            return $user->role == 'participant';
        });

        $gate->define('premium-access', function ($user) {
            return $user->membership == 'gold' || $user->membership == 'platinum';
        });
    }
}
