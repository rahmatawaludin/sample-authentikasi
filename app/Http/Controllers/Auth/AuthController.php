<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use JWTAuth;
use JWTException;

class AuthController extends Controller
{

    protected $username = 'username';

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'role' => 'in:organizer,participant',
            'membership' => 'in:silver,gold,platinum'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role' => isset($data['role']) ? $data['role'] : 'participant',
            'membership' => isset($data['membership']) ? $data['membership'] : 'silver'
        ]);
    }

    /**
     * Redirect the user to the provider authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        try {
            return \Socialite::driver($provider)->redirect();
        } catch (InvalidArgumentException $e) {
            return abort(404, 'Driver tidak dikenal.');
        }
    }

    /**
     * Obtain the user information from provider
     *
     * @return Response
     */
    public function providerCallback($provider)
    {
        try {
            $account = \Socialite::driver($provider)->user();
            $user = User::firstOrCreate(['provider' => $provider, 'provider_id' => $account->id]);
            // update details
            $user->name = $account->name;
            $user->email = $account->email;
            $user->avatar = $account->avatar;
            $user->save();

            \Auth::login($user, true);
            return redirect('home');

        } catch (InvalidArgumentException $e) {
            return abort(404, 'Driver tidak dikenal.');
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return redirect('auth/login');
        }
    }

    public function getToken(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }
}
