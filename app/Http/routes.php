<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', ['middleware' => 'auth', 'uses' => 'HomeController@index']);

Route::get('/settings', ['middleware' => 'auth', function () {
    return "Menampilkan halaman settings user.";
}]);

Route::get('event', ['middleware' => ['auth', 'role:organizer'], function() {
    return "Berhasil mengakses halaman event";
}]);

Route::get('event-history', ['middleware' => ['auth', 'role:participant'], function() {
    return "Berhasil mengakses history event.";
}]);

Route::get('edit-event/{id}', 'HomeController@editEvent');
Route::get('edit-organization/{id}', 'HomeController@editOrganization');
Route::get('join-event/{id}', 'HomeController@joinEvent');

Route::get('settings', ['middleware' => 'auth', 'uses' => 'HomeController@settings']);

Route::get('premium', ['middleware' => ['auth'], 'uses' => 'HomeController@premium']);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// Socialite Auth
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@providerCallback');

// JWT
Route::group(['prefix' => 'api'], function() {
    Route::post('authenticate', 'Auth\AuthController@getToken');
    Route::group(['middleware' => 'jwt.auth'], function() {
        Route::get('users', function() {
            return App\User::all();
        });
    });
});

