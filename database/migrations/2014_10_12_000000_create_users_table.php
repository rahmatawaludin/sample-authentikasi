<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('role');

            $table->string('name');
            $table->string('email');
            $table->string('password', 60);

            $table->string('provider');
            $table->string('provider_id')->unique()->nullable();
            $table->string('avatar');

            $table->boolean('suspended')->default(0);
            $table->string('membership');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
